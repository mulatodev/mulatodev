### Hola!!!, Soy Gustavo

Soy un **Desarrollador Backend en Java y C#**. Tengo un grado de **Ingeniería Informática** y especialización en **Gerencia de Proyectos**. He participado y acompañado equipos de desarrollo de software en empresas como **Intergrupo** (hoy SoftwareOne), **Grupo EPM** y **Grupo Sura** (sumando 12 años de experiencia en ellas), entre otras. Me identifico como un entusiasta por aprender **siempre** sobre nuevos lenguajes de programación y tecnologías.

Algunos hobbies que practico son: Montar bici, leer, bailar, fotografía y no menos importante, pasar tiempo de calidad con mi esposa e hija. Nos encanta viajar!!!.

### Contáctame en
| [mail](mailto:ganaranjo@gmail.com)
| [linkedin](https://www.linkedin.com/in/gustavonaranjobenavides)
|
